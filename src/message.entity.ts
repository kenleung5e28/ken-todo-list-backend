import { Entity, PrimaryKey, Property } from '@mikro-orm/core'

@Entity({ tableName: 'testing' })
export class Message {
  @PrimaryKey()
  id!: string;

  @Property({ name: 'author_id' })
  authorId!: number;

  @Property()
  message!: string;

  constructor(authorId: number, message: string) {
    this.authorId = authorId;
    this.message = message;
  }
}