import { MikroORM } from '@mikro-orm/core';
import { EntityManager } from '@mikro-orm/postgresql';
import { Injectable } from '@nestjs/common';
import { Message } from './message.entity';

@Injectable()
export class AppService {
  constructor(private readonly orm: MikroORM, private readonly em: EntityManager) {
    this.orm = orm;
    this.em = em;
  }

  async getHello(): Promise<string[]> {
    const repo = this.orm.em.getRepository(Message);
    const msgs = await repo.findAll();
    return msgs.map(x => x.message);
  }
}
