import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Message } from './message.entity';
@Module({
  imports: [
    MikroOrmModule.forRoot({
      entities: [Message],
      type: 'postgresql',
      clientUrl: 'postgresql://kenleung5e28@127.0.0.1:5432',
      dbName: 'todo_list_db',
      user: 'kenleung5e28',
      password: 'student'
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
